package com.integrado.trestambores.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.integrado.trestambores.R;
import com.integrado.trestambores.model.Passadas;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Button btnCompetidores = findViewById(R.id.btCompetidores);
//
//        btnCompetidores.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), CompetidoresActivity.class);
//                startActivity(intent);
//            }
//        });
    }

    public void abreTelaCompetidores(View view){
        Intent intent = new Intent(this, CompetidoresActivity.class);
        startActivity(intent);
    }

    public void abreTelaPassadas(View view){
        Intent intent = new Intent(this, PassadasActivity.class);
        startActivity(intent);
    }
}
