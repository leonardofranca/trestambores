package com.integrado.trestambores.activity;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.integrado.trestambores.R;
import com.integrado.trestambores.helper.CompetidorDAO;
import com.integrado.trestambores.model.Competidor;

public class AdicionarCompetidorActivity extends AppCompatActivity {

    private TextInputEditText editCompetidor;
    private Competidor competidorAtual;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar_competidor);

        editCompetidor = findViewById(R.id.textCompetidor);

        //Recuperar competidor, caso seja edição
        competidorAtual = (Competidor) getIntent().getSerializableExtra("tarefaSelecionada");

        //Configurar a caixa de texto
        if (competidorAtual != null){
            editCompetidor.setText(competidorAtual.getNomeCompetidor());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_adicionar_competidor, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemSalvar:
                //Executa ação para o item salvar
                CompetidorDAO competidorDAO = new CompetidorDAO(getApplicationContext());

                if (competidorAtual != null){//edicao
                    String nomeCompetidor = editCompetidor.getText().toString();
                    if (!nomeCompetidor.isEmpty()){

                        Competidor competidor = new Competidor();
                        competidor.setNomeCompetidor(nomeCompetidor);
                        competidor.setId(competidorAtual.getId());

                        //atualizar no banco de dados
                        if (competidorDAO.atualizar(competidor)){
                            finish();
                            Toast.makeText(getApplicationContext(),
                                    "Sucesso ao atualizar competidor!",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Erro ao atualizar competidor!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {//salvar

                    String nomeCompetidor = editCompetidor.getText().toString();
                    if (!nomeCompetidor.isEmpty()){
                        Competidor competidor = new Competidor();
                        competidor.setNomeCompetidor(nomeCompetidor);

                        if (competidorDAO.salvar(competidor)){
                            finish();
                            Toast.makeText(getApplicationContext(),
                                    "Sucesso ao salvar competidor!",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Erro ao salvar competidor!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}