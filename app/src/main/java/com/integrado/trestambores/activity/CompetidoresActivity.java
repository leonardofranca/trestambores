package com.integrado.trestambores.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.integrado.trestambores.R;
import com.integrado.trestambores.adapter.CompetidorAdapter;
import com.integrado.trestambores.helper.RecyclerItemClickListener;
import com.integrado.trestambores.helper.CompetidorDAO;
import com.integrado.trestambores.model.Competidor;

import java.util.ArrayList;
import java.util.List;

public class CompetidoresActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CompetidorAdapter competidorAdapter;
    private List<Competidor> listaCompetidores = new ArrayList<>();
    private Competidor competidorSelecionado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_competidor_adapter);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Confgurar recycler
        recyclerView = findViewById(R.id.recyclerView);

        //opcional para testar version
//            DbHelper db = new DbHelper( getApplicationContext() );
//
//            //dados para inserir
//            ContentValues cv = new ContentValues();
//            cv.put("nome", "Teste");
//
//            //escrever dados no banco
//            db.getWritableDatabase().insert("competidores", null, cv);


        //Adicionar evento de clique
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                //Recuperar competidor para edicao
                                Competidor competidorSelecionada = listaCompetidores.get( position );

                                //Enviar competidor para tela adicionar competidor
                                Intent intent = new Intent( CompetidoresActivity.this, AdicionarCompetidorActivity.class );
                                intent.putExtra("competidorSelecionada", competidorSelecionada);

                                startActivity( intent );
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                                //Recuperar competidor para deletar
                                competidorSelecionado = listaCompetidores.get( position );

                                AlertDialog.Builder dialog = new AlertDialog.Builder(CompetidoresActivity.this);

                                //configurar título e mensagem
                                dialog.setTitle("Confirmar exclusão");
                                dialog.setMessage("Desejar excluir a competidor: " + competidorSelecionado.getNomeCompetidor() + "?");

                                //configurar botoes
                                dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        CompetidorDAO competidorDAO = new CompetidorDAO( getApplicationContext() );
                                        if ( competidorDAO.deletar(competidorSelecionado) ){

                                            carregarListaCompetidores();
                                            Toast.makeText(getApplicationContext(),
                                                    "Sucesso ao deletar competidor!",
                                                    Toast.LENGTH_SHORT).show();

                                        } else {

                                            Toast.makeText(getApplicationContext(),
                                                    "Erro ao deletar competidor!",
                                                    Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });

                                dialog.setNegativeButton("Não", null);

                                //Exibir dialog
                                dialog.create();
                                dialog.show();

                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AdicionarCompetidorActivity.class);
                startActivity(intent);
            }
        });
    }

    public void carregarListaCompetidores(){
        //Listar competidores
        CompetidorDAO competidorDAO = new CompetidorDAO( getApplicationContext() );
        listaCompetidores = competidorDAO.listar();

        /*
         *   Exibe lista de competidores no Recyclerview
         */
        //Configurar um adapter
        competidorAdapter = new CompetidorAdapter(listaCompetidores);

        //Configurar RecyclerView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager( layoutManager );
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        recyclerView.setAdapter(competidorAdapter);
    }

    @Override
    protected void onStart() {
        carregarListaCompetidores();
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
