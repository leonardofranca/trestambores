package com.integrado.trestambores.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Passadas implements Serializable {

    private Long id;
    private Double tempo;
    private Integer com_id;
    private String nomeCompetidor;
    private List<Passadas> listaPassadas = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTempo() {
        return tempo;
    }

    public void setTempo(Double tempo) {
        this.tempo = tempo;
    }

    public Integer getCom_id() {
        return com_id;
    }

    public void setCom_id(Integer com_id) {
        this.com_id = com_id;
    }

    public String getNomeCompetidor() {
        return nomeCompetidor;
    }

    public void setNomeCompetidor(String nomeCompetidor) {
        this.nomeCompetidor = nomeCompetidor;
    }
}
