package com.integrado.trestambores.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Competidor implements Serializable {

    private Long id;
    private String nomeCompetidor;
    private List<Competidor> listaCompetidor = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeCompetidor() {
        return nomeCompetidor;
    }

    public void setNomeCompetidor(String nomeCompetidor) {
        this.nomeCompetidor = nomeCompetidor;
    }
}
