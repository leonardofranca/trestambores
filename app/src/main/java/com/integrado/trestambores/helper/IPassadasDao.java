package com.integrado.trestambores.helper;

import com.integrado.trestambores.model.Passadas;

import java.util.List;

public interface IPassadasDao {
    public boolean salvar(Passadas passadas);
    public boolean atualizar(Passadas passadas);
    public boolean deletar(Passadas passadas);
    public List<Passadas> listar();
}
