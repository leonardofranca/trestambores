package com.integrado.trestambores.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.integrado.trestambores.model.Passadas;

import java.util.ArrayList;
import java.util.List;

public class PassadasDao implements IPassadasDao {

    private SQLiteDatabase escreve;
    private SQLiteDatabase le;

    public PassadasDao(Context context ) {
        DbHelper db = new DbHelper(context);
        escreve = db.getWritableDatabase();
        le = db.getReadableDatabase();
    }

    @Override
    public boolean salvar(Passadas passadas) {
        ContentValues cv = new ContentValues();
        cv.put("tempo", passadas.getTempo().toString());
        cv.put("com_id", passadas.getCom_id().toString());

        try{
            escreve.insert(DbHelper.TABELA_PASSADAS, null, cv);
            Log.i("RESULTADO", "Passada salva  com sucesso");
        } catch (Exception e){
            Log.i("RESULTADO", "Erro ao salva passada");
            return false;
        }
        return true;
    }

    @Override
    public boolean atualizar(Passadas passadas) {
        ContentValues cv = new ContentValues();
        cv.put("tempo", passadas.getTempo().toString());

        try{
            String[] args = { passadas.getId().toString() };
            escreve.update(DbHelper.TABELA_PASSADAS, cv, "id=?", args);
            Log.i("RESULTADO", "Sucesso ao atualizar passada");
        } catch (Exception e) {
            Log.i("RESULTADO", "Erro ao atualizar passada");
            return false;

        }
        return true;
    }

    @Override
    public boolean deletar(Passadas passadas) {
        return false;
    }

    @Override
    public List<Passadas> listar() {
        List<Passadas> passadas = new ArrayList<>();
        String sql = "SELECT * FROM " + DbHelper.TABELA_PASSADAS +
                        " INNER JOIN competidores comp ON (comp.id = passadas.com_id);";
        Cursor c = le.rawQuery(sql, null);

        while (c.moveToNext() ){
            Passadas passada = new Passadas();

            Long id = c.getLong(c.getColumnIndex("id"));
            Double tempo = c.getDouble(c.getColumnIndex("tempo"));
            String nomeCompetidor = c.getString(c.getColumnIndex("nome"));

            passada.setId(id);
            passada.setTempo(tempo);
            passada.setNomeCompetidor(nomeCompetidor);

            passadas.add(passada);
        }
        return passadas;
    }
}
